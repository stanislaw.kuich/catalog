# Catalog
Zbiór projektów modelujących katalog wspierany bazą danych.
## Technologie i projekty
### pcatalog
Python + unittest/pytest
### jcatalog
Java + Junit 5, Mockito
### ncatalog
TypeScipt + NestJS, Jest
