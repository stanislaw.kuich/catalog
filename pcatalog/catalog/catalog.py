import catalog.person


class Catalog(object):

    def __init__(self, db, service):
        self.db = db
        self.service = service

    def get_pesel(self, name, surname):
        return self.db.get_pesel_list(name, surname)[0]

    def get_pesel_list(self, name, surname):
        return self.db.get_pesel_list(name, surname)

    def add_person(self, name, surname, pesel):
        if self.service.verify(pesel):
            p = catalog.person.Person(name, surname, pesel)
            self.db.add_person(p)
        else:
            raise Exception("improper PESEL")

    def get_person(self, pesel):
        return self.db.get_person(pesel)